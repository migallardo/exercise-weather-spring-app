package com.mg.exerciseweatherspringapp.dataprovider.client;

import com.mg.exerciseweatherspringapp.dataprovider.domain.DarkSkyRs;
import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(value = "weatherClient", url = "${dark-sky.url}/${dark-sky.key}")
public interface WeatherClient {

  @RequestLine("GET /{latitude},{longitude}?lang=es&units=si")
  DarkSkyRs getWeatherStatus(
      @Param("latitude") String latitude, @Param("longitude") String longitude);
}
