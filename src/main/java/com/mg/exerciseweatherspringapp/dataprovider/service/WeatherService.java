package com.mg.exerciseweatherspringapp.dataprovider.service;

import com.mg.exerciseweatherspringapp.dataprovider.client.WeatherClient;
import com.mg.exerciseweatherspringapp.dataprovider.translator.WeatherTranslator;
import com.mg.exerciseweatherspringapp.entrypoints.domain.request.WeatherRq;
import com.mg.exerciseweatherspringapp.entrypoints.domain.response.WeatherRs;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class WeatherService {

  private final WeatherClient weatherClient;
  private final WeatherTranslator weatherTranslator;

  @Autowired
  WeatherService(WeatherClient weatherClient, WeatherTranslator weatherTranslator) {
    this.weatherClient = weatherClient;
    this.weatherTranslator = weatherTranslator;
  }

  public WeatherRs getWeather(WeatherRq request) {
    var country = weatherTranslator.getCountry(request);
    return weatherTranslator.translateResponse(
        weatherClient.getWeatherStatus(country.getLat(), country.getLon()), country);
  }
}
