package com.mg.exerciseweatherspringapp.dataprovider.exceptions;

import com.mg.exerciseweatherspringapp.entrypoints.domain.response.WeatherRs;
import java.util.UUID;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestControllerAdvice extends ResponseEntityExceptionHandler {
  @ExceptionHandler
  protected ResponseEntity<WeatherRs> handleRuntimeException(Exception ex, WebRequest request) {
    return new ResponseEntity<>(
        WeatherRs.builder()
            .requestId(UUID.randomUUID().toString())
            .status(HttpStatus.CONFLICT)
            .debugMessage(ex.getMessage())
            .build(),
        new HttpHeaders(),
        HttpStatus.CONFLICT);
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
      MethodArgumentNotValidException ex,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request) {
    return new ResponseEntity<>(
        WeatherRs.builder()
            .requestId(UUID.randomUUID().toString())
            .status(HttpStatus.BAD_REQUEST)
            .debugMessage(ex.getMessage())
            .build(),
        new HttpHeaders(),
        HttpStatus.BAD_REQUEST);
  }
}
