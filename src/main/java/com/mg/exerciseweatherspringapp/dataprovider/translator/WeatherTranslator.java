package com.mg.exerciseweatherspringapp.dataprovider.translator;

import com.mg.exerciseweatherspringapp.dataprovider.domain.CountryInfo;
import com.mg.exerciseweatherspringapp.dataprovider.domain.Currently;
import com.mg.exerciseweatherspringapp.dataprovider.domain.Daily;
import com.mg.exerciseweatherspringapp.dataprovider.domain.DarkSkyRs;
import com.mg.exerciseweatherspringapp.dataprovider.domain.Weather;
import com.mg.exerciseweatherspringapp.entrypoints.domain.request.WeatherRq;
import com.mg.exerciseweatherspringapp.entrypoints.domain.response.CurrentTime;
import com.mg.exerciseweatherspringapp.entrypoints.domain.response.DayStatus;
import com.mg.exerciseweatherspringapp.entrypoints.domain.response.WeatherData;
import com.mg.exerciseweatherspringapp.entrypoints.domain.response.WeatherRs;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@Log4j2
public class WeatherTranslator {

  private static final String DATE_FORMAT = "dd-MM-yyyy";
  private static final String DATE_TIME_FORMAT = "dd-MM-yyyy HH:mm:ss";
  private static final String COUNTRY_NOT_FOUND_ERROR_MESSAGE = "Country %s not supported";

  @Resource
  @Qualifier("countries")
  private Map<String, CountryInfo> countryMap;

  public CountryInfo getCountry(WeatherRq request) {

    var countryCode =
        Optional.ofNullable(request.getCountry()).map(String::toUpperCase).orElseGet(String::new);

    return Optional.ofNullable(countryMap.get(countryCode))
        .orElseThrow(
            () ->
                new RuntimeException(String.format(COUNTRY_NOT_FOUND_ERROR_MESSAGE, countryCode)));
  }

  public WeatherRs translateResponse(DarkSkyRs darkSkyResponse, CountryInfo countryInfo) {

    DarkSkyRs response = Optional.ofNullable(darkSkyResponse).orElseGet(DarkSkyRs::new);

    return WeatherRs.builder()
        .requestId(UUID.randomUUID().toString())
        .data(
            WeatherData.builder()
                .country(countryInfo.getCountry())
                .city(countryInfo.getCity())
                .current(buildCurrentTime(response.getCurrently(), response.getTimezone()))
                .week(getDaysOfWeek(response.getDaily(), response.getTimezone()))
                .build())
        .build();
  }

  protected CurrentTime buildCurrentTime(Currently currently, String timeZone) {
    Currently currentWeather = Optional.ofNullable(currently).orElseGet(Currently::new);

    LocalDateTime date =
        LocalDateTime.ofInstant(
            Instant.ofEpochSecond(currentWeather.getTime().longValue()), ZoneId.of(timeZone));

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);

    return CurrentTime.builder()
        .time(date.format(formatter))
        .status(currentWeather.getSummary())
        .temperature(currentWeather.getTemperature())
        .chanceOfRaining(currentWeather.getPrecipProbability())
        .build();
  }

  protected List<DayStatus> getDaysOfWeek(Daily dailyData, String timeZone) {
    Daily daily = Optional.of(dailyData).orElseGet(Daily::new);
    List<Weather> weatherData = Optional.ofNullable(daily.getData()).orElseGet(ArrayList::new);

    return weatherData.stream()
        .filter(Objects::nonNull)
        .map(data -> buildDayOfWeek(data, timeZone))
        .collect(Collectors.toList());
  }

  protected DayStatus buildDayOfWeek(Weather weatherData, String timeZone) {
    LocalDate date =
        LocalDate.ofInstant(
            Instant.ofEpochSecond(weatherData.getTime().longValue()), ZoneId.of(timeZone));

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);

    return DayStatus.builder()
        .day(
            StringUtils.capitalize(
                date.getDayOfWeek().getDisplayName(TextStyle.FULL_STANDALONE, Locale.getDefault())))
        .date(date.format(formatter))
        .status(weatherData.getSummary())
        .minTemperature(weatherData.getTemperatureMin())
        .maxTemperature(weatherData.getTemperatureMax())
        .chanceOfRaining(weatherData.getPrecipProbability())
        .build();
  }
}
