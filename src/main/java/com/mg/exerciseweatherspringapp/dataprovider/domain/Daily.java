package com.mg.exerciseweatherspringapp.dataprovider.domain;

import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Daily implements Serializable {
  public String summary;
  public String icon;
  public List<Weather> data;
}
