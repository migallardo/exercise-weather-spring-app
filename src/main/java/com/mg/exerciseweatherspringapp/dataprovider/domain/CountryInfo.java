package com.mg.exerciseweatherspringapp.dataprovider.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CountryInfo {

  private String country;
  private String city;
  private String lat;
  private String lon;
}
