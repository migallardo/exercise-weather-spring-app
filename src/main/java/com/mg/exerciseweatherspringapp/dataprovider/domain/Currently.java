package com.mg.exerciseweatherspringapp.dataprovider.domain;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Currently implements Serializable {
  private Integer time;
  private String summary;
  private String icon;
  private Integer precipIntensity;
  private Integer precipProbability;
  private Double temperature;
  private Double apparentTemperature;
  private Double dewPoint;
  private Double humidity;
  private Double pressure;
  private Double windSpeed;
  private Double windGust;
  private Integer windBearing;
  private Double cloudCover;
  private Integer uvIndex;
  private Double visibility;
  private Integer ozone;
}
