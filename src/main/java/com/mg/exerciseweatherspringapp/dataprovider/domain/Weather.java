package com.mg.exerciseweatherspringapp.dataprovider.domain;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Weather implements Serializable {
  private Integer time;
  private String summary;
  private String icon;
  private Integer precipIntensity;
  private Integer precipProbability;
  private Double temperature;
  private Double apparentTemperature;
  private Double dewPoint;
  private Double humidity;
  private Double pressure;
  private Double windSpeed;
  private Double windGust;
  private Integer windBearing;
  private Double cloudCover;
  private Integer uvIndex;
  private Double visibility;
  private Double ozone;
  private Integer sunriseTime;
  private Integer sunsetTime;
  private Double moonPhase;
  private Double precipIntensityMax;
  private Double temperatureHigh;
  private Integer temperatureHighTime;
  private Double temperatureLow;
  private Integer temperatureLowTime;
  private Double apparentTemperatureHigh;
  private Integer apparentTemperatureHighTime;
  private Double apparentTemperatureLow;
  private Integer apparentTemperatureLowTime;
  private Integer windGustTime;
  private Integer uvIndexTime;
  private Double temperatureMin;
  private Integer temperatureMinTime;
  private Double temperatureMax;
  private Integer temperatureMaxTime;
  private Double apparentTemperatureMin;
  private Integer apparentTemperatureMinTime;
  private Double apparentTemperatureMax;
  private Integer apparentTemperatureMaxTime;
  private Integer precipIntensityMaxTime;
  private String precipType;
}
