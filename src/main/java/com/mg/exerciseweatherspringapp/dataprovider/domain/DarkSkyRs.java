package com.mg.exerciseweatherspringapp.dataprovider.domain;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DarkSkyRs implements Serializable {
  private Double latitude;
  private Double longitude;
  private String timezone;
  private Currently currently;
  private Hourly hourly;
  private Daily daily;
  public Integer offset;
}
