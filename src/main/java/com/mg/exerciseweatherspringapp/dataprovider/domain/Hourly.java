package com.mg.exerciseweatherspringapp.dataprovider.domain;

import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Hourly implements Serializable {
  private String summary;
  private String icon;
  private List<Weather> data;
}
