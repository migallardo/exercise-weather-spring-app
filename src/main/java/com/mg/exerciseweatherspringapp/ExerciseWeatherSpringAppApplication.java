package com.mg.exerciseweatherspringapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ExerciseWeatherSpringAppApplication {

  public static void main(String[] args) {
    SpringApplication.run(ExerciseWeatherSpringAppApplication.class, args);
  }
}
