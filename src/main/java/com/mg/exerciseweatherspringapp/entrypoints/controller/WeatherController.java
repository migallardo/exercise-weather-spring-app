package com.mg.exerciseweatherspringapp.entrypoints.controller;

import com.mg.exerciseweatherspringapp.dataprovider.service.WeatherService;
import com.mg.exerciseweatherspringapp.entrypoints.domain.request.WeatherRq;
import com.mg.exerciseweatherspringapp.entrypoints.domain.response.WeatherRs;
import javax.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/weather")
@Log4j2
public class WeatherController {

  private final WeatherService weatherService;

  @Autowired
  WeatherController(WeatherService weatherService) {
    this.weatherService = weatherService;
  }

  @PostMapping
  public WeatherRs checkWeather(@RequestBody @Valid WeatherRq request) {
    log.info("{} => checkWeather with Request {}", getClass().getSimpleName(), request);
    return weatherService.getWeather(request);
  }
}
