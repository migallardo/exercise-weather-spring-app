package com.mg.exerciseweatherspringapp.entrypoints.domain.response;

import java.io.Serializable;
import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
@Builder
public class WeatherRs implements Serializable {
  private String requestId;
  private HttpStatus status;
  private String debugMessage;
  private WeatherData data;
}
