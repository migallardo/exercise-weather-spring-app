package com.mg.exerciseweatherspringapp.entrypoints.domain.response;

import java.io.Serializable;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DayStatus implements Serializable {
  private String day;
  private String date;
  private String status;
  private Double minTemperature;
  private Double maxTemperature;
  private Integer chanceOfRaining;
}
