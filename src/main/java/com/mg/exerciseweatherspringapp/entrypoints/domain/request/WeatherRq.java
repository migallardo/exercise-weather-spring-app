package com.mg.exerciseweatherspringapp.entrypoints.domain.request;

import java.io.Serializable;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WeatherRq implements Serializable {

  @NotEmpty(message = "Country can't be empty")
  private String country;
}
