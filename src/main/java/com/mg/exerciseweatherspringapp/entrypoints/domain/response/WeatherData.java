package com.mg.exerciseweatherspringapp.entrypoints.domain.response;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WeatherData {
  private String country;
  private String city;
  private CurrentTime current;
  private List<DayStatus> week;
}
