package com.mg.exerciseweatherspringapp.entrypoints.domain.response;

import java.io.Serializable;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CurrentTime implements Serializable {
  private String time;
  private String status;
  private Double temperature;
  private Integer chanceOfRaining;
}
