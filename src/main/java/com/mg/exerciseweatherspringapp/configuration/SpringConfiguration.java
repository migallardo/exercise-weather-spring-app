package com.mg.exerciseweatherspringapp.configuration;

import com.mg.exerciseweatherspringapp.dataprovider.domain.CountryInfo;
import feign.Contract;
import java.util.HashMap;
import java.util.Map;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringConfiguration {

  @Bean
  public Contract useFeignAnnotations() {
    return new Contract.Default();
  }

  @Bean(name = "countries")
  @ConfigurationProperties(prefix = "country-map")
  public Map<String, CountryInfo> getCountryInfo() {
    return new HashMap<>();
  }
}
